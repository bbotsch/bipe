const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const bipe = b.addModule("bipe", .{
        .root_source_file = b.path("src/bipe.zig"),
    });

    const lib = b.addStaticLibrary(.{
        .name = "bipe",
        .root_source_file = b.path("src/libbipe.zig"),
        .target = target,
        .optimize = optimize,
    });
    lib.root_module.addImport("bipe", bipe);
    lib.linkLibC();

    const exe = b.addExecutable(.{
        .name = "bipe",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });
    exe.root_module.addImport("bipe", bipe);

    const exe_c = b.addExecutable(.{
        .name = "bipe_c",
        .target = target,
        .optimize = optimize,
    });
    exe_c.addCSourceFile(.{ .file = b.path("src/main.c") });
    exe_c.linkLibrary(lib);

    b.installArtifact(lib);
    b.installArtifact(exe);
    b.installArtifact(exe_c);
    b.installFile("src/libbipe.h", "include/bipe.h");

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const unit_tests = b.addTest(.{
        .root_source_file = b.path("src/bipe.zig"),
        .target = target,
        .optimize = optimize,
    });

    const run_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}
