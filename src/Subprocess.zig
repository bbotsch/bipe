const std = @import("std");
const posix = std.posix;
const Subprocess = @This();

pid: posix.pid_t,

pub var shellpath: [:0]const u8 = "/bin/sh";

const Options = struct {
    cmd: ?[*:0]const u8 = null,
    stdin: ?posix.fd_t = null,
    stdout: ?posix.fd_t = null,
    closefds: []const posix.fd_t = &[0]posix.fd_t{},
};
pub fn spawn(allocator: std.mem.Allocator, opts: Options) !Subprocess {
    const argv: [*:null]const ?[*:0]const u8 = if (opts.cmd) |cmd|
        &[_:null]?[*:0]const u8{ shellpath.ptr, "-c", cmd }
    else
        &[_:null]?[*:0]const u8{shellpath.ptr};

    const envp = try allocator.allocSentinel(?[*:0]const u8, std.os.environ.len, null);
    defer allocator.free(envp);
    for (envp, std.os.environ) |*e, v| {
        e.* = v;
    }

    const pid = try posix.fork();
    if (pid == 0) {
        if (opts.stdin) |stdin| {
            posix.close(posix.STDIN_FILENO);
            try posix.dup2(stdin, posix.STDIN_FILENO);
        }
        if (opts.stdout) |stdout| {
            posix.close(posix.STDOUT_FILENO);
            try posix.dup2(stdout, posix.STDOUT_FILENO);
        }
        for (opts.closefds) |fd| {
            posix.close(fd);
        }
        return posix.execveZ(shellpath, argv, envp);
    }
    return Subprocess{ .pid = pid };
}

pub fn wait(this: Subprocess) void {
    _ = posix.waitpid(this.pid, 0);
}

test {
    const p = try Subprocess.spawn(std.testing.allocator, .{ .cmd = "true" });
    p.wait();
}
