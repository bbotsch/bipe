#include "libbipe.h"
#include <sys/wait.h>
#include <stdio.h>

int main(int argc, char** argv)
{
    struct ReturnProcesses bipe_processes = bipe(argv[1], argv[2]);
    if (bipe_processes.p1 == 0) {
        fprintf(stderr, "Error spawning subprocess 1\n");
    }
    if (bipe_processes.p2 == 0) {
        fprintf(stderr, "Error spawning subprocess 2\n");
    }

    waitpid(bipe_processes.p1, NULL, 0);
    waitpid(bipe_processes.p2, NULL, 0);
}
