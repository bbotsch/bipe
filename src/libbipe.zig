const std = @import("std");
const posix = std.posix;
const bipe_zig = @import("bipe");

export fn bipe(cmd1: [*:0]const u8, cmd2: [*:0]const u8) extern struct {
    p1: posix.pid_t = 0,
    p2: posix.pid_t = 0,
} {
    // We need to setup std.os.environ ourselves because we might not be called from a zig executable
    var environ_len: u16 = 0;
    while (std.c.environ[environ_len] != null) environ_len += 1;
    std.os.environ = @as([][*:0]u8, @ptrCast(std.c.environ[0..environ_len]));

    const subprocesses = bipe_zig.bipe(cmd1, cmd2, std.heap.c_allocator) catch return .{};
    return .{
        .p1 = subprocesses.p1.pid,
        .p2 = subprocesses.p2.pid,
    };
}
