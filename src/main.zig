const std = @import("std");
const bipe = @import("bipe");
const args = @import("args.zig");

pub fn main() !void {
    try args.parseArgs();

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    if (std.posix.getenv("SHELL")) |shell| {
        bipe.Subprocess.shellpath = shell;
    }

    const bipe_processes = try bipe.bipe(args.cmd1, args.cmd2, allocator);

    const check = gpa.deinit();
    std.debug.assert(check == .ok);

    if (args.dontwait) {
        const stdout = std.io.getStdOut().writer();
        try stdout.print("Processes successfully biped\n", .{});
        std.process.exit(0);
    }

    bipe_processes.p1.wait();
    bipe_processes.p2.wait();
}
