const std = @import("std");
const posix = std.posix;
pub const Subprocess = @import("Subprocess.zig");

pub fn bipe(cmd1: [*:0]const u8, cmd2: [*:0]const u8, allocator: std.mem.Allocator) !struct {
    p1: Subprocess,
    p2: Subprocess,
} {
    const forward_pipe = try posix.pipe();
    const reverse_pipe = try posix.pipe();

    const p1 = try Subprocess.spawn(allocator, .{
        .cmd = cmd1,
        .stdin = reverse_pipe[0],
        .stdout = forward_pipe[1],
        .closefds = &[_]posix.fd_t{ reverse_pipe[1], forward_pipe[0] },
    });

    const p2 = try Subprocess.spawn(allocator, .{
        .cmd = cmd2,
        .stdin = forward_pipe[0],
        .stdout = reverse_pipe[1],
        .closefds = &[_]posix.fd_t{ forward_pipe[1], reverse_pipe[0] },
    });

    posix.close(forward_pipe[0]);
    posix.close(forward_pipe[1]);
    posix.close(reverse_pipe[0]);
    posix.close(reverse_pipe[1]);

    return .{ .p1 = p1, .p2 = p2 };
}

test {
    const bipe_processes = try bipe("env", "cat", std.testing.allocator);
    bipe_processes.p1.wait();
    bipe_processes.p2.wait();
}
