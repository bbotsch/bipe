const std = @import("std");

var progname: [:0]const u8 = undefined;

pub var dontwait = false;
pub var cmd1: [:0]const u8 = undefined;
pub var cmd2: [:0]const u8 = undefined;

fn usage() void {
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    defer bw.flush() catch {};
    const stdout = bw.writer();

    stdout.print(
        \\Usage: {s} [OPTION]... CMD1 CMD2
        \\Run two shell commands with their stdouts and stdins connected to each other.
        \\CMD1 and CMD2 are run in the environment's shell.
        \\The name comes from BIdirectional piPE.
        \\
        \\  -h, --help	display this help and exit
        \\  -w, --dontwait	do not wait for subprocesses to finish before exiting
        \\
    , .{progname}) catch {};
}

pub fn parseArgs() error{WrongArgs}!void {
    var args = std.process.args();
    defer args.deinit();

    progname = args.next().?;

    var arg = args.next() orelse {
        usage();
        return error.WrongArgs;
    };
    if (std.mem.eql(u8, arg, "-h") or std.mem.eql(u8, arg, "--help")) {
        usage();
        std.process.exit(0);
    } else if (std.mem.eql(u8, arg, "-w") or std.mem.eql(u8, arg, "--dontwait")) {
        dontwait = true;
        arg = args.next() orelse {
            usage();
            return error.WrongArgs;
        };
    }

    cmd1 = arg;

    cmd2 = args.next() orelse {
        usage();
        return error.WrongArgs;
    };

    if (args.skip()) {
        usage();
        return error.WrongArgs;
    }
}
