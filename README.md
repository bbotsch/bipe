# Description
Bipe is a simple program that allows you to run two shells commands and pipe the outputs of each to the other. It is called bipe because it is a BIdirection piPE.
Bipe achieves the same effect as the following shell script, but without needing to use a separate named pipe:

```
#!/bin/bash
PIPE=/tmp/bipe
mkfifo $PIPE
$1 < $PIPE | $2 > $PIPE
```

This may be useful when trying to forward a network connections somewhere else with the help of netcat.
For example, you can do something like this:

`$ bipe "nc -U /tmp/server.sock" "nc 192.168.1.11 3000"`

The bipe process can be made to exit as soon as the subprocesses are set up with the -w/--dontwait option so it is as transparent as possible. Otherwise, the bipe process will exit when both commands have exited.

# How to build
Bipe is written in Zig (https://ziglang.org/) and uses the zig build system.
Building is as simple as issuing `zig build -Doptimize=ReleaseSmall`
This will produce:

| File      | Description                                                               |
| --------- | ------------------------------------------------------------------------- |
| libbipe.a | C ABI-compatible static library written in zig (see libbipe.zig)          |
| bipe.h    | C API header file                                                         |
| bipe_c    | example C implementation of bipe using the libbipe.a library (see main.c) |
| bipe      | fully-featured zig implementation of bipe (see main.zig)                  |

# License
This project and all files associated with it is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
